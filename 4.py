def test_alternate():
    # Test case 1
    s = "abacaba"
    expected_output = 7
    assert alternate(s) == expected_output

    # Test case 2
    s = "abcdabcd"
    expected_output = 4
    assert alternate(s) == expected_output

    # Test case 3
    s = "abcabcabc"
    expected_output = 3
    assert alternate(s) == expected_output

test_alternate()