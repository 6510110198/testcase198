import unittest

class TestAlternatingCharacters(unittest.TestCase):
    def test_example(self):
        s = 'AAAA'
        result = alternatingCharacters(s)
        self.assertEqual(result, 3)
        
        s = 'BBBBB'
        result = alternatingCharacters(s)
        self.assertEqual(result, 4)
        
        s = 'ABABABAB'
        result = alternatingCharacters(s)
        self.assertEqual(result, 0)
        
        s = 'AAABBB'
        result = alternatingCharacters(s)
        self.assertEqual(result, 4)
        
if __name__ == '__main__':
    unittest.main()