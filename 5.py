import io
import sys

def run_io_fun(input_data, fun):
    old_stdin = sys.stdin
    old_stdout = sys.stdout
    sys.stdin = io.StringIO(input_data)
    sys.stdout = io.StringIO()
    func = fun
    func()
    sys.stdin = old_stdin
    sys.stdout = old_stdout
    return sys.stdout.getvalue().strip()

input_data = '''2
3
ebacd
fghij
olmkn
5
abcde
fghij
klmno
pqrst
uvwxy
'''

expected_output = '''YES
NO
'''

assert run_io_fun(input_data, gridChallenge) == expected_output