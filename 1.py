import unittest

class TestFunnyString(unittest.TestCase):
    def test_funny_string(self):
        self.assertEqual(funnyString("acxz"), "Funny")
        self.assertEqual(funnyString("bcxz"), "Not Funny")
        self.assertEqual(funnyString("abcdefghijklmnopqrstuvwxyz"), "Funny")
        self.assertEqual(funnyString("abcdefghijklmnopqrstuvwxyz"[::-1]), "Funny")

if __name__ == '__main__':
    unittest.main()